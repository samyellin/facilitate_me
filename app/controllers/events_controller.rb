class EventsController < ApplicationController
  def sort
    params[:order].each do |key,value|
      Event.find(value[:id]).update_attribute(:priority,value[:position])
    end
    render :nothing => true
  end
end
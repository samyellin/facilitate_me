class ActivitiesController < ApplicationController
  before_action :set_activity, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!, only: [:update_difficulties, :edit, :update, :destroy, :create, :new]
  before_action :verify_user, only: [:edit, :update, :destroy]
  
  # GET /activities
  # GET /activities.json
  def index
    @activities = Activity.all
  end

  # GET /activities/1
  # GET /activities/1.json
  def show
  end

  # GET /activities/new
  def new
    @activity = Activity.new
  end

  # GET /activities/1/edit
  def edit
  end

  # POST /activities
  # POST /activities.json
  def create
    @activity = Activity.new(activity_params)
    respond_to do |format|
      if @activity.save
        current_user.activities << @activity
        format.html { redirect_to @activity, notice: 'Activity was successfully created.' }
        format.json { render :show, status: :created, location: @activity }
      else
        format.html { render :new }
        format.json { render json: @activity.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /activities/1
  # PATCH/PUT /activities/1.json
  def update
    respond_to do |format|
      if @activity.update(activity_params)
        format.html { redirect_to @activity, notice: 'Activity was successfully updated.' }
        format.json { render :show, status: :ok, location: @activity }
      else
        format.html { render :edit }
        format.json { render json: @activity.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /activities/1
  # DELETE /activities/1.json
  def destroy
    @activity.destroy
    respond_to do |format|
      format.html { redirect_to activities_url, notice: 'Activity was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def result
    if params[:search] == ""
      @activities = Activity.search "*"
    else
      @activities = Activity.search params[:search],  page: params[:page], fields: ["title^10", "classification^8", "outcomes^6", "processing" ]
    end
  end

  def update_difficulties
    submitted_physical_difficulty = params[:physicalDifficulty].to_f
    submitted_interpersonal_difficulty = params[:interpersonalDifficulty].to_f
    submitted_emotional_difficulty = params[:emotionalDifficulty].to_f
    @activity = Activity.find(params[:id])
    if @activity.total_votes
      @activity.total_votes += 1
      @activity.physical_difficulty = ((@activity.physical_difficulty * (@activity.total_votes - 1)) + submitted_physical_difficulty / 5) / @activity.total_votes 
      @activity.interpersonal_difficulty = ((@activity.interpersonal_difficulty * (@activity.total_votes - 1)) + submitted_interpersonal_difficulty / 5) / @activity.total_votes
      @activity.emotional_difficulty = ((@activity.emotional_difficulty * (@activity.total_votes - 1)) + submitted_emotional_difficulty / 5) / @activity.total_votes
    else 
      @activity.total_votes = 1
      @activity.physical_difficulty = submitted_physical_difficulty / 5.0
      @activity.interpersonal_difficulty = submitted_interpersonal_difficulty / 5.0
      @activity.emotional_difficulty = submitted_emotional_difficulty / 5.0
    end
    
    @activity.save
    
    respond_to do |format|
      format.js
    end
  end
  
  private
    # Use callbacks to share common setup or constraints between actions.
    def set_activity
      @activity = Activity.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def activity_params
      params.require(:activity).permit(:title, :classification, :supplies, :outcomes, :setup, :procedure, :modifications, :processing, :notes)
    end
    
    def verify_user
      @activity = Activity.find(params[:id])
      if (@activity.user_id != current_user.id) && !current_user.admin
        flash[:notice]  = "You do not have permission to do that."
        redirect_to activities_path
      end
    end
end

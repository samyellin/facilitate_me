class SequencesController < ApplicationController
  before_action :set_sequence, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!, only: [:edit, :update, :destroy, :create, :new]
  before_action :verify_user, only: [:edit, :update, :destroy]
  
  def index
    @sequences = Sequence.all
  end
  
  def show
    @sequence = Sequence.find(params[:id])
    @events = @sequence.events.all
  end
  
  def edit
  end

  def new
    @sequence = Sequence.new
  end
  
  def create
    @sequence = Sequence.new(sequence_params)
    respond_to do |format|
      if @sequence.save
        current_user.sequences << @sequence
        format.html { redirect_to select_path(:id => @sequence.id), notice: 'Sequence was successfully created.' }
      else
        format.html { render :new }
      end
    end
  end
  
  def update
    respond_to do |format|
      if @sequence.update(sequence_params)
        format.html { redirect_to @sequence, notice: 'Sequence was successfully updated.' }
      else
        format.html { render :edit }
      end
    end
  end
 
  def destroy
    @sequence.destroy
    respond_to do |format|
      format.html { redirect_to sequences_url, notice: 'Sequence was successfully destroyed.' }
    end
  end 

  def select_activities
    @sequence = Sequence.find(params[:id])
    @activities = Activity.all
    @events = @sequence.events.all
  end

  def add_selection
    @sequence = Sequence.find(params[:sequence_id])
    @activity = Activity.find(params[:activity_id])
    @event = Event.new(:priority => 1)
    @event.activity = @activity
    @event.sequence = @sequence
    @event.priority = 1
    @event.save
    @events = @sequence.events.all

    respond_to do |format|
      format.js
    end
  end

  def remove_selection
    @sequence = Sequence.find(params[:sequence_id])
    @event = @sequence.events.find(params[:event_id])
    @event.destroy
    @sequence.reload
    @events = @sequence.events.all
    
    respond_to do |format|
      format.js
    end
  end

  def update_result
    @sequence = Sequence.find(params[:sequence_id])
    if params[:search] == ""
      @activities = Activity.search "*"
    else
      @activities = Activity.search params[:search],  page: params[:page], fields: ["title^10", "classification^8", "outcomes^6", "processing" ]
    end
    
    respond_to do |format|
      format.js
    end
  end
  
  private
    # Use callbacks to share common setup or constraints between actions.
    def set_sequence
      @sequence = Sequence.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def sequence_params
      params.require(:sequence).permit(:title, :description)
    end
    
    def verify_user
      @sequence = Sequence.find(params[:id])
      if (@sequence.user_id != current_user.id) && !current_user.admin
        flash[:notice]  = "You do not have permission to do that."
        redirect_to sequences_path
      end
    end
end

# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
$ ->
  $(document).on 'click', '#vote-submit', (evt) ->
    physical_difficulty = $("#physical-difficulty-rating").val()
    interpersonal_difficulty = $("#interpersonal-difficulty-rating").val()
    emotional_difficulty = $("#emotional-difficulty-rating").val()
    if (physical_difficulty < 1 or physical_difficulty > 5) or (interpersonal_difficulty < 1 or interpersonal_difficulty > 5) or (emotional_difficulty < 1 or emotional_difficulty > 5)
        alert 'Scores must be between 1 and 5.'
    else
        $.ajax 'update_difficulties',
        type: 'POST'
        dataType: 'script'
        data: {
            physicalDifficulty: physical_difficulty
            interpersonalDifficulty: interpersonal_difficulty
            emotionalDifficulty: emotional_difficulty
            id: $("#activity-id").val()
        }
        error: (jqXHR, textStatus, errorThrown) ->
            console.log("AJAX Error: #{textStatus}")
        success: (data, textStatus, jqXHR) ->
            console.log("Dynamic vote submission OK!")
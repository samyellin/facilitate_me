$(document).on 'click', '#search-submit', (evt) ->
    evt.preventDefault()
    result = $("#search-terms").val()
    id = $("#sequence-id").val()
    $.ajax 'update_result',
    type: 'GET'
    dataType: 'script'
    data: {
        search: result
        sequence_id: id
    }
    error: (jqXHR, textStatus, errorThrown) ->
        console.log("AJAX Error: #{textStatus}")
    success: (data, textStatus, jqXHR) ->
        console.log("Dynamic search OK!") 
        
$(document).on 'click', '.activity-selection', (evt) ->
    evt.preventDefault()
    $.ajax 'add_selection',
    type: 'POST'
    dataType: 'script'
    data: {
        activity_id: $(this).attr('activity_id')
        sequence_id: $(this).attr('sequence_id')
    }
    error: (jqXHR, textStatus, errorThrown) ->
        console.log("AJAX Error: #{textStatus}")
    success: (data, textStatus, jqXHR) ->
        ready()
        console.log("Dynamic activity addition OK!") 
        
$(document).on 'click', '.remove-activity', (evt) ->
    evt.preventDefault()
    $.ajax 'remove_selection',
    type: 'POST'
    dataType: 'script'
    data: {
        event_id: $(this).attr('event_id')
        sequence_id: $(this).attr('sequence_id')
    }
    error: (jqXHR, textStatus, errorThrown) ->
        console.log("AJAX Error: #{textStatus}")
    success: (data, textStatus, jqXHR) ->
        ready()
        console.log("Dynamic activity removal OK!")         

ready = undefined
set_positions = undefined

set_positions = ->
  # loop through and give each task a data-pos
  # attribute that holds its position in the DOM
  $('.event-box').each (i) ->
    $(this).attr 'data-pos', i + 1
    return
  return

ready = ->
  # call set_positions function
    set_positions()
    $('.sortable').sortable()
 
    $('.sortable').sortable().bind 'sortupdate', (e, ui) ->
        # array to store new order
        updated_order = []
        # set the updated positions
        set_positions()
        # populate the updated_order array with the new task positions
        $('.event-box').each (i) ->
            updated_order.push
                id: $(this).data('id')
                position: i + 1
            return
        # send the updated order via ajax
        $.ajax
            type: 'PUT'
            url: '/events/sort'
            data: order: updated_order
        return
    return

$(document).ready ready
$("#vote-results").empty()
  .append("<%= escape_javascript(render :partial => 'ratings') %>")
$("#vote-form").empty()
  .append("Thank you for your input!")
json.extract! @activity, :id, :title, :classification, :supplies, :outcomes, :setup, :procedure, :modifications, :processing, :notes, :created_at, :updated_at

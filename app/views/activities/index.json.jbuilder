json.array!(@activities) do |activity|
  json.extract! activity, :id, :title, :classification, :supplies, :outcomes, :setup, :procedure, :modifications, :processing, :notes
  json.url activity_url(activity, format: :json)
end

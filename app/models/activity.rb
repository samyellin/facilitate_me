class Activity < ApplicationRecord
    belongs_to :submittor, :class_name => "User",:foreign_key => "user_id"
    has_many :events
    validates :title, :classification, :supplies, :outcomes,
        :setup, :procedure, :modifications, :processing, presence: true
    validates :title, uniqueness: { case_sensitive: false }
    validates :title, length:{ minimum: 2, maximum: 80 }
    
    searchkick
    acts_as_commontable
end

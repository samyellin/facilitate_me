class Sequence < ApplicationRecord
    has_many :events
    belongs_to :submittor, :class_name => "User",:foreign_key => "user_id"
    validates :title, presence: true, length: { maximum: 100 }
    validates :description, presence: true, length: { maximum: 100 }
end

class Event < ApplicationRecord
    belongs_to :sequence
    belongs_to :activity
    
    default_scope { order("priority ASC") }
end

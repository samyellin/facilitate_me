module ActivitiesHelper
    def getPhysicalDifficulty
        if @activity.physical_difficulty
            return @activity.physical_difficulty
        else
            return 0
        end
    end
    
    def getInterpersonalDifficulty
        if @activity.interpersonal_difficulty
            return @activity.interpersonal_difficulty
        else
            return 0
        end
    end
    
    def getEmotionalDifficulty
        if @activity.emotional_difficulty
            @activity.emotional_difficulty
        else
            return 0
        end
    end
end

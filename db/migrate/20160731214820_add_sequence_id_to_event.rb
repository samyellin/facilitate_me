class AddSequenceIdToEvent < ActiveRecord::Migration[5.0]
  def change
    add_column :events, :sequence_id, :integer
  end
end

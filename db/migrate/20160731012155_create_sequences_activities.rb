class CreateSequencesActivities < ActiveRecord::Migration[5.0]
  def change
    create_table :sequences_activities do |t|
      t.integer :sequence_id
      t.integer :activity_id
    end
  end
end

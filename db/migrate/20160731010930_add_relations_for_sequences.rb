class AddRelationsForSequences < ActiveRecord::Migration[5.0]
  def change
    add_reference :sequences, :user, foreign_key: true
  end
end

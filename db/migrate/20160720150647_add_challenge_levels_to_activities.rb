class AddChallengeLevelsToActivities < ActiveRecord::Migration[5.0]
  def change
    add_column :activities, :physical_difficulty, :integer
    add_column :activities, :interpersonal_difficulty, :integer
    add_column :activities, :emotional_difficulty, :integer
  end
end

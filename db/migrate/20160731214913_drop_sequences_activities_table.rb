class DropSequencesActivitiesTable < ActiveRecord::Migration[5.0]
  def change
    drop_table :sequences_activities
  end
end

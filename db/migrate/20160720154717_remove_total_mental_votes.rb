class RemoveTotalMentalVotes < ActiveRecord::Migration[5.0]
  def change
    remove_column :activities, :total_mental_votes
  end
end

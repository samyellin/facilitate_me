class FixDifficulties < ActiveRecord::Migration[5.0]
  def change
    change_column :activities, :physical_difficulty, :float
    change_column :activities, :interpersonal_difficulty, :float
    change_column :activities, :emotional_difficulty, :float
    add_column :activities, :total_physical_votes, :integer
    add_column :activities, :total_interpersonal_votes, :integer
    add_column :activities, :total_mental_votes, :integer
  end
end

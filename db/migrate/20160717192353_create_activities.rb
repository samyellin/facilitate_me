class CreateActivities < ActiveRecord::Migration[5.0]
  def change
    create_table :activities do |t|
      t.string :title
      t.string :classification
      t.string :supplies
      t.text :outcomes
      t.text :setup
      t.text :procedure
      t.text :modifications
      t.text :processing
      t.text :notes

      t.timestamps
    end
  end
end

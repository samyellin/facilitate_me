Rails.application.routes.draw do
  resources :activities
  resources :sequences
  devise_for :users
  mount Commontator::Engine => '/commontator'
  root 'static_pages#home'
  get 'about' => 'static_pages#about'
  get 'result' => 'activities#result'
  post 'activities/update_difficulties', as: 'update_difficulties'
  get 'select' => 'sequences#select_activities'
  post 'add_selection' => 'sequences#add_selection'
  post 'remove_selection' => 'sequences#remove_selection'
  get 'update_result' => 'sequences#update_result'
  resources :events do
    put :sort, on: :collection
  end
end
